<?php

if (!function_exists('pr')) {

    function pr($e) {
        echo "<pre>";
        print_r($e);
        echo "</pre>";
    }

}

if (!function_exists('vd')) {

    function vd($e) {
        echo "<pre>";
        var_dump($e);
        echo "</pre>";
    }

}