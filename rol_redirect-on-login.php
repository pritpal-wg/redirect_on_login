<?php

/**
 * Plugin Name: Force Password Reset on First Login
 * Version: 1.0
 * Description: Redirects the users to password reset page on first login
 */
@session_start();
require_once('functions.php');
require_once __DIR__ . '/PageTemplater/classPageTemplater.php';
require_once __DIR__ . '/PageTemplater/classTemplatepages.php';

class RedirectOnLogin {

    function __construct() {
        if (isset($_SESSION['rol_password_updated'])) {
            unset($_SESSION['rol_password_updated']);
            add_filter('login_message', array($this, 'password_changed'));
        }
        add_filter('login_redirect', array($this, 'redirectOnFirstLogin'), 10, 3);
        add_filter('get_header', array($this, 'redirectOnFirstLogin1'));
        add_filter('admin_init', array($this, 'redirectOnFirstLogin1'));
    }

    function password_changed() {
        return '<p class="message">Password changed successfully, Login with your new password</p>';
    }

    function redirectOnFirstLogin($redirect_to, $requested_redirect_to, $user) {

        // If they're on the login page, don't do anything
        if (!isset($user->user_login)) {
            return $redirect_to;
        }

        //get reset page link
        {
            $resetPage = ROL_Template_Pages::getPage();
            if (!$resetPage)
                return $redirect_to;
        }

        // How many times to redirect the user
        $num_redirects = 1;
        // If implementing this on an existing site, this is here so that existing users don't suddenly get the "first login" treatment
        // On a new site, you might remove this setting and the associated check
        // Alternative approach: run a script to assign the "already redirected" property to all existing users
        // Alternative approach: use a date-based check so that all registered users before a certain date are ignored
        // 172800 seconds = 48 hours
//        $message_period = 60 * 60;
        $message_period = 60 * 60 * 24 * 2; //2 days

        $key_name = 'rol_redirect_on_first_login';
        // Third parameter ensures that the result is a string
        $current_redirect_value = get_user_meta($user->ID, $key_name, true);
        if (strtotime($user->user_registered) > ( time() - $message_period ) && ( '' == $current_redirect_value || intval($current_redirect_value) < $num_redirects )
        ) {
            return $resetPage;
        } else {
            return $redirect_to;
        }
    }

    function redirectOnFirstLogin1() {
        //if on password reset page then no redirect
        if (!is_user_logged_in())
            return;

        //get reset page link
        {
            $resetPage = ROL_Template_Pages::getPage();
            if (!$resetPage)
                return;
        }
        global $user_ID;
        $user = get_user_by('id', $user_ID);

        $num_redirects = 1;
        $message_period = 60 * 60 * 24 * 2; //2 days

        $key_name = 'rol_redirect_on_first_login';
        // Third parameter ensures that the result is a string
        $current_redirect_value = get_user_meta($user->ID, $key_name, true);
        if (strtotime($user->user_registered) > ( time() - $message_period ) && ( '' == $current_redirect_value || intval($current_redirect_value) < $num_redirects )
        ) {
            wp_redirect($resetPage);
            die;
//            return $resetPage;
        }
    }

}

new RedirectOnLogin;
?>