<?php

if (!defined('ABSPATH'))
    return;

class PageTemplater {

    /**
     * A reference to an instance of this class.
     */
    private static $instance;

    /**
     * The array of templates that this plugin tracks.
     */
    protected $templates;

    /**
     * Returns an instance of this class.
     */
    public static function get_instance() {
        if (null == self::$instance) {
            self::$instance = new PageTemplater();
        }
        return self::$instance;
    }

    /**
     * Initializes the plugin by setting filters and administration functions.
     */
    private function __construct() {
        $this->templates = array();
        // Add a filter to the attributes metabox to inject template into the cache.
        add_filter(
                'page_attributes_dropdown_pages_args', array($this, 'register_project_templates')
        );
        // Add a filter to the save post to inject out template into the page cache
        add_filter(
                'wp_insert_post_data', array($this, 'register_project_templates')
        );
        // Add a filter to the template include to determine if the page has our
        // template assigned and return it's path
        add_filter(
                'template_include', array($this, 'view_project_template')
        );

        //remove page editor when page template is selected
        {
            add_action('init', array($this, 'remove_page_editor'));
        }
        // Add your templates to this array.
        $this->templates = array(
            'rol_passwordreset-page.php' => 'Password Reset Page',
        );
    }

    /**
     * Remove the Page Editor when the template is selected
     */
    public function remove_page_editor() {
        if (!is_admin()) {
            return;
        }

        // Get the post ID on edit post with filter_input super global inspection.
        $current_post_id = filter_input(INPUT_GET, 'post', FILTER_SANITIZE_NUMBER_INT);
        // Get the post ID on update post with filter_input super global inspection.
        $update_post_id = filter_input(INPUT_POST, 'post_ID', FILTER_SANITIZE_NUMBER_INT);

        // Check to see if the post ID is set, else return.
        if (isset($current_post_id)) {
            $post_id = absint($current_post_id);
        } else if (isset($update_post_id)) {
            $post_id = absint($update_post_id);
        } else {
            return;
        }

        if (isset($post_id)) {
            // Get the template of the current post.
            $template_file = get_post_meta($post_id, '_wp_page_template', true);

            if (array_key_exists($template_file, $this->templates)) {
                add_action('edit_form_after_title', function() {
                    global $post;
                    $template_file = get_post_meta($post->ID, '_wp_page_template', true);
                    echo "<br /><br /><br /><br /><center><h3>Page Editor is disabled for Template - <i style=\"font-weight: normal\">" . $this->templates[$template_file] . "</i></h3></center>";
                });
                remove_post_type_support('page', 'editor');
            }
        }
        //remove_post_type_support( 'page', 'editor' );
    }

    /**
     * Adds our template to the pages cache in order to trick WordPress
     * into thinking the template file exists where it doens't really exist.
     *
     */
    public function register_project_templates($atts) {
        // Create the key used for the themes cache
        $cache_key = 'page_templates-' . md5(get_theme_root() . '/' . get_stylesheet());
        // Retrieve the cache list.
        // If it doesn't exist, or it's empty prepare an array
        $templates = wp_get_theme()->get_page_templates();
        if (empty($templates)) {
            $templates = array();
        }
        // New cache, therefore remove the old one
        wp_cache_delete($cache_key, 'themes');
        // Now add our template to the list of templates by merging our templates
        // with the existing templates array from the cache.
        $templates = array_merge($templates, $this->templates);
        // Add the modified cache to allow WordPress to pick it up for listing
        // available templates
        wp_cache_add($cache_key, $templates, 'themes', 1800);
        return $atts;
    }

    /**
     * Checks if the template is assigned to the page
     */
    public function view_project_template($template) {
        global $post;
        if (!isset($this->templates[get_post_meta($post->ID, '_wp_page_template', true)])) {
            return $template;
        }
        $file = plugin_dir_path(__FILE__) . 'templates/' . get_post_meta($post->ID, '_wp_page_template', true);

        // Just to be safe, we check if the file exist first
        if (file_exists($file))
            return $file;
        else
            echo $file;
        return $template;
    }

}

add_action('plugins_loaded', array('PageTemplater', 'get_instance'));
