<?php
@session_start();
if (!is_user_logged_in())
    wp_redirect(wp_login_url());
if (isset($_POST['rol_submit'])) {
    $pass = trim($_POST['pass']);
    $c_pass = trim($_POST['c_pass']);
    $error = false;
    $error_desc = array();
    if (strpos($pass, ' ') !== FALSE) {
        $error = TRUE;
        $error_desc[] = 'Blank space is not allowed in New Password';
    }
    if (!$error) {
        if (!$pass) {
            $error = TRUE;
            $error_desc[] = 'New Password can\'t be empty';
        }
        if ($pass !== $c_pass) {
            $error = TRUE;
            $error_desc[] = 'Passwords didn\'t match, Try Again.';
        }
    }
    if (!$error) {
        global $user_ID;
        wp_set_password($pass, $user_ID);
        $key_name = 'rol_redirect_on_first_login';
        $current_redirect_value = get_user_meta($user_ID, $key_name, TRUE);
        if (!$current_redirect_value)
            $current_redirect_value = 0;
        update_user_meta($user_ID, $key_name, $current_redirect_value + 1);
        $_SESSION['rol_password_updated'] = TRUE;
        wp_redirect(wp_login_url());
        die;
    }
}

add_filter('the_content', array('ROL_Template_Pages', 'passwordreset_page'));
?>
<div style="height: 200px;background: #0f0f0f;color:#f0f0f0;line-height: 200px;text-align: center;margin-bottom: 40px">
    <h1><?php the_title() ?></h1>
</div>
<div style="text-align: center;">
    <?php the_content() ?>
</div>