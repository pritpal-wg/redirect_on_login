<?php

class ROL_Template_Pages {

    public static function getPage($page = 'rol_passwordreset', $get_id = false) {
        if (!is_string($page))
            return;
        global $wpdb;
        if (strpos($page, '-page.php') === FALSE)
            $page .= "-page.php";
        $sql = "SELECT post_id FROM `{$wpdb->postmeta}` WHERE meta_key='_wp_page_template' and meta_value='$page'";
        $page_id = $wpdb->get_row($sql);
        if ($page_id) {
            $page_id = $page_id->post_id;
            $page_link = get_permalink($page_id);
            return $get_id === TRUE ? (int) $page_id : $page_link;
        }
        return;
    }

    function passwordreset_page($content) {
        global $post;
        $page_name = 'rol_passwordreset';
        if ($post->ID !== self::getPage($page_name, true))
            return $content;
        global $user_ID, $error, $error_desc;
        ob_start();
        ?>
        <form method="post" style="text-align: center;">
            <table style="width: 500px;text-align: center;margin: 0 auto;border: solid 1px;padding-top: 20px">
                <tbody>
                    <tr style="padding: 5px">
                        <th style="padding: 5px">New Password</th>
                        <td style="padding: 5px">
                            <input type="password" name="pass" autocomplete="off" required />
                        </td>
                    </tr>
                    <tr style="padding: 5px">
                        <th style="padding: 5px">Confirm Password</th>
                        <td style="padding: 5px">
                            <input type="password" name="c_pass" autocomplete="off" required />
                        </td>
                    </tr>
                    <tr style="padding: 5px">
                        <th style="padding: 5px" colspan="2">
                            <input type="submit" name="rol_submit" value="Done" />
                        </th>
                    </tr>
                </tbody>
                <?php if (isset($error) && !empty($error_desc)) { ?>
                    <tfoot>
                        <tr>
                            <td colspan="2" style="padding: 5px">
                                <ul style="list-style-type: none;padding: 0;margin: 0;color: tomato">
                                    <?php foreach ($error_desc as $err) { ?>
                                        <li><?php echo $err ?></li>
                                    <?php } ?>
                                </ul>
                            </td>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>
        </form>
        <?php
        return ob_get_clean();
    }

}
